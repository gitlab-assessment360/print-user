# Print User

Write a Ruby or Bash script that will print usernames of all users on a Linux system together with their home directories. Here's some example output:

## Assessment Objective

1. Ruby or Bash script
2. Print usernames
3. Linux system
